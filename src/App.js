import {Canvas} from '@react-three/fiber'
import React, {Suspense, useState} from 'react'
import PuzzleBoard from "./components/PuzzleBoard";
import Menu from "./components/Menu"

export default function App({bgColor}) {

    const [menu, setMenu,isMenu] = useState(false)
    return (
        <>
            <Canvas gl={{antialias: false, alpha: false}} mode="concurrent" dpr={[1, 1.5]}
                    style={{background: `${bgColor}`}}>
                <ambientLight intensity={0.5}/>
                <spotLight position={[10, 10, 10]} angle={0.15} penumbra={1}/>
                <pointLight position={[-10, -10, -10]}/>
                <Suspense fallback={null}>

                    {/*<Menu/>*/}
                    <PuzzleBoard/>


                </Suspense>
            </Canvas>
        </>

    )
}
