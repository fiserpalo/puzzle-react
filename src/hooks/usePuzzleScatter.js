import {useMemo} from "react";
import * as THREE from "three";
import Card from "../components/Card";
import {Vector3} from "three";
import {BoxModel} from "../components/BoxModel";

import bear from "../textures/pexeso/bear.jpg";
import owl from "../textures/pexeso/owl.jpg";
import cat from "../textures/pexeso/cat.jpg";
import snake from "../textures/pexeso/snake.jpg";
import whale from "../textures/pexeso/whale.jpg";
import monkey from "../textures/pexeso/monkey.jpg";
import wolf from "../textures/pexeso/wolf.jpg";
import lion from "../textures/pexeso/lion.jpg";
import panda from "../textures/pexeso/panda.jpg";


import bearText from "../textures/pexeso/bear_0.jpg";
import owlText from "../textures/pexeso/owl_0.jpg";
import catText from "../textures/pexeso/cat_0.jpg";
import snakeText from "../textures/pexeso/snake_0.jpg";
import whaleText from "../textures/pexeso/whale_0.jpg";
import monkeyText from "../textures/pexeso/monkey_0.jpg";
import wolfText from "../textures/pexeso/wolf_0.jpg";
import lionText from "../textures/pexeso/lion_0.jpg";
import pandaText from "../textures/pexeso/panda_0.jpg";

export default function usePuzzleScatter() {

    function shuffle(array) {
        let newArray = [];
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            newArray[i] = array[j];
            newArray[j] = temp;
        }
        return newArray;
    }

    const textures = [
        [
            bear,
            bearText
        ],
        [
            owl,
            owlText
        ],
        [
            cat,
            catText
        ],
        [
            snake,
            snakeText
        ],
        [
            wolf,
            wolfText
        ],
        [
            lion,
            lionText
        ],
        [
            whale,
            whaleText
        ],
        [
            monkey,
            monkeyText
        ],
        [
            panda,
            pandaText
        ]

    ]


    const points = useMemo(() => {
        let pts = [];
        let gap = 0.8;

        let square = 4;
        let q = 0;
        for (let y = 1; y <= square; y++) {
            for (let x = 1; x <= square; x += 2) {
                q++;
                let picOne = textures[q][0];
                let picTwo = textures[q][1];
                let cardTwo;
                let cardOne;
                // let cardTwo = [
                //     key => 'card_' + y + '-' + x,
                //     pairCard => cardTwo,
                //     sidePic => picTwo,
                //     position => new Vector3(x * gap, y * gap - 2, -2)
                // ];
                // let cardOne = [
                //     key => 'card_' + y + '-' + x,
                //     pairCard => cardTwo,
                //     sidePic => picOne,
                //     position => new Vector3(x * gap, y * gap - 2, -2)
                // ];
                cardOne = <Card key={'card_' + y + '-' + x} pairCard={cardTwo} sidePic={picOne}
                                position={[x * gap, y * gap - 2, -2]}/>;
                cardTwo = <Card key={'card_' + (y + 1) + '-' + (x + 1)} pairCard={cardOne} sidePic={picTwo}
                                position={[(x + 1) * gap, (y) * gap - 2, -2]}/>;
                pts.push(cardOne);
                pts.push(cardTwo);
            }
        }

        return pts.sort(() => Math.random() - 0.5)
    });

    return points;
}


