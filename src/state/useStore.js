import { Color } from 'three'
import { createRef } from 'react'
import create from 'zustand'

const useStore = create((set, get) => {

  return {
    set,
    get,
    gameOver: false,
    gameStarted: false,
    hasInteracted: false,
    setHasInteracted: () => set(state => ({ hasInteracted: true })),
    setScore: (score) => set(state => ({ score: score })),
    setGameStarted: (started) => set(state => ({ gameStarted: started })),
    setGameOver: (over) => set(state => ({ gameOver: over })),
  }
})

const mutation = {
  gameOver: false,
}

export { useStore, mutation }