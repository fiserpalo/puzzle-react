import React from "react";
import ReactDOM from "react-dom";
import './styles.css'
import App from "./App";

ReactDOM.render(<App bgColor='#141622' />, document.getElementById('root'))

// const rootElement = document.getElementById("root");
// ReactDOM.render(
//     <React.StrictMode>
//         <App />
//     </React.StrictMode>,
//     rootElement
// );
