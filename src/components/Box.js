import React, {useRef, useState} from 'react'
import {useFrame, useLoader} from '@react-three/fiber'
import {TextureLoader} from "three";
import * as THREE from "three";

export default function Box(props) {

    // const texture_1 = useLoader(TextureLoader, '../textures/cube/dice_1.jpg')
    // const texture_2 = useLoader(TextureLoader, 'textures/cube/dice_2.jpg')
    // const texture_3 = useLoader(TextureLoader, 'textures/cube/dice_3.jpg')
    // const texture_4 = useLoader(TextureLoader, 'textures/cube/dice_4.jpg')
    // const texture_5 = useLoader(TextureLoader, 'textures/cube/dice_5.jpg')
    // const texture_6 = useLoader(TextureLoader, 'textures/cube/dice_6.jpg')

    // This reference gives us direct access to the THREE.Mesh object
    const ref = useRef()
    // Hold state for hovered and clicked events
    const [hovered, hover] = useState(false)
    const [clicked, click] = useState(false)
    const [flipped, flip] = useState(false)

    const getColor = () => {
        return flipped ? 'red' : hovered ? 'green' : 'orange';
    }
    const flipEvent = (event) => {
        flip(!flipped);
    }

    const rotate = () => {
        if (flipped) {
            if (ref.current.rotation.x < Math.PI) {
                ref.current.rotation.x += 0.3;
            } else {
                ref.current.rotation.x = Math.PI;
            }

        } else {
            if (ref.current.rotation.x > 0) {
                ref.current.rotation.x -= 0.3;
            } else {
                ref.current.rotation.x = 0;
            }
        }

    }

    useFrame((state, delta) => (rotate()))

    return (
        <mesh
            {...props}
            ref={ref}
            scale={clicked ? 1.5 : 1}
            onClick={flipEvent}
            onPointerOver={(event) => hover(true)}
            onPointerOut={(event) => hover(false)}>
            <boxGeometry args={[1, 1, 0.1]}/>
            <meshStandardMaterial color={getColor()}/>
            {/*<meshStandardMaterial map={texture_1} attach="material" />*/}
            {/*<meshStandardMaterial map={texture_2} attach="material" />*/}
            {/*<meshStandardMaterial map={texture_3} attach="material" />*/}
            {/*<meshStandardMaterial map={texture_4} attach="material" />*/}
            {/*<meshStandardMaterial map={texture_5} attach="material" />*/}
            {/*<meshStandardMaterial map={texture_6} attach="material" />*/}
        </mesh>
    )
}