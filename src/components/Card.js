import React, {useRef, Suspense, useState} from 'react'
import {Canvas, useFrame, useLoader} from '@react-three/fiber'
import {TextureLoader} from 'three/src/loaders/TextureLoader.js'
import * as THREE from "three";
import metal from "../textures/download.jpeg";


export default function Card(props) {

    const loader = new THREE.CubeTextureLoader();
    loader.setPath('textures/');

    const base = new THREE.TextureLoader().load(metal);
    const sidePic = new THREE.TextureLoader().load(props.sidePic);

    const mesh = useRef()

    const [hovered, hover] = useState(false)
    const [clicked, click] = useState(false)
    const [flipped, flip] = useState(false)

    const getColor = () => {
        return flipped ? '' : hovered ? '' : '';
    }
    const getMap = (mesh) => {

        return !flipped ? base : sidePic;
    }
    const flipEvent = (event) => {
        flip(!flipped);

        // console.log(props.pairCard);
    }

    const rotate = () => {
        if (flipped) {
            if (mesh.current.rotation.x < Math.PI) {
                mesh.current.rotation.x += 0.2;
            } else {
                mesh.current.rotation.x = Math.PI;
            }

        } else {
            if (mesh.current.rotation.x > 0) {
                mesh.current.rotation.x -= 0.2;
            } else {
                mesh.current.rotation.x = 0;
            }
        }
    }

    useFrame((state, delta) => (rotate()))
    return (
        <mesh ref={mesh} scale={0.7}
              position={props.position}
              onClick={flipEvent}
              onPointerOver={(event) => hover(true)}
              onPointerOut={(event) => hover(false)}>
            <boxBufferGeometry  args={[1, 1, 0.1]} attach="geometry"/>
            <meshBasicMaterial  attach="material" map={getMap()} color={getColor()} />
        </mesh>
    )
}

