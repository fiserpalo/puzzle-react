import * as THREE from "three";
import Card from "./Card";

export class BoxModel {
    constructor(x, y, z, name) {
        this.position = new THREE.Vector3(x, y, z);
        this.name = name;
        this.flipped = false;
        this.pairBox = null;
        this.getBox = () => {
            return "<Card  key={this.name} position={this.position} />";
        }
    }


}